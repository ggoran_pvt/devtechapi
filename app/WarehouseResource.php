<?php
/**
 * Created by PhpStorm.
 * User: night5talker
 * Date: 17.01.17.
 * Time: 19:02
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PDO;

class WarehouseResource extends Model {

    protected $db;
    // add DB fields that can be written on
    protected $fillable = ['name', 'locationId', 'managerId'];

    public function __construct() {
        if ($this->db == null) {
            $this->db = new PDO('mysql:host=localhost;dbname=devtechAPI', 'root', 'mysql');
        }
        return $this->db;
    }


    // show all entries
    public function indexWarehouse() {
        // sql to fetch all entries
        $sql = $this->db->prepare("SELECT WarehouseResource.*,Location.longitude, Location.latitude FROM WarehouseResource INNER JOIN Location ON WarehouseResource.locationId = Location.id");
        $sql->execute();
        $data['data'] = $sql->fetchAll(PDO::FETCH_ASSOC);

        $data['response_code'] = 200;

        return $data;
    }

    // show single entry
    public function showWarehouse($id) {
        // find single entry
        $sql = $this->db->prepare("SELECT WarehouseResource.*,Location.longitude, Location.latitude FROM WarehouseResource INNER JOIN Location ON WarehouseResource.locationId = Location.id WHERE WarehouseResource.id = :id");
        $sql->execute(array(
            ':id' => $id
        ));

        if ($sql->rowCount() == 0) {
            $data['data'] = "Page not found";
            $data['response_code'] = 404;
        } else {
            $data['data'] = $sql->fetch(PDO::FETCH_ASSOC);
            $data['response_code'] = 200;
        }

        Log::info('Showing data:'.implode("|",$data['data']));

        return $data;
    }

    // create entry
    public function createWarehouse($request) {

        $latitude = $request['latitude'];
        $longitude = $request['longitude'];

        // get location ID
        $locationId = Location::getLocationId($longitude, $latitude);

        // add to warehouse
        $warehouse = DB::table('WarehouseResource')->insertGetId([
            'name' => $request['name'],
            'managerId' => $request['managerId'],
            'locationId' => $locationId
        ]);

        if ($warehouse != null) {
            $data['data'] = $this->showWarehouse($warehouse);
            $data['id'] = $warehouse;
            $data['response_code'] = 200;
            Log::info('created new entry with id:'.$data['id']);
        }
        else{
            $data['data'] = "Invalid request";
            $data['response_code'] = 400;
            Log::error('Bad request incoming '.$request);
        }
        return $data;
    }

    // delete entry
    public function deleteWarehouse($id) {
        $sql = DB::table('WarehouseResource')->where('id', '=', $id)->count();

        // delete
        if ($sql == 1) {
            DB::table('WarehouseResource')->where('id', '=', $id)->delete();
            $data['data'] = "ID:$id removed";
            $data['response_code'] = 200;
            Log::info('erased entry with id:'.$data['id']);
        } else {
            $data['data'] = "ID:$id not found";
            $data['response_code'] = 404;
        }
        return $data;
    }

    // update entry
    public function updateWarehouse($request, $id) {
        $latitude = $request['latitude'];
        $longitude = $request['longitude'];
        // check location
        $locationId = Location::getLocationId($longitude, $latitude);

        // update entry
        DB::table('WarehouseResource')
            ->where('id', '=', $id)
            ->update([
                'name' => $request['name'],
                'managerId' => $request['managerId'],
                'locationId' => $locationId
            ]);

        $data['data'] = "ID:$id updated";
        $data['response_code'] = 200;

        return $data;
    }


}