<?php
/**
 * Created by PhpStorm.
 * User: night5talker
 * Date: 18.01.17.
 * Time: 17:47
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Location extends Model {

    public static function getLocationId($longitude, $latitude) {
        $model = new self();

        $locationId = $model->doesLocationExists($longitude, $latitude);

        return $locationId;
    }


    public function doesLocationExists($longitude, $latitude) {
        // search for location in database
        $sql = DB::table('Location')
            ->where('longitude', '=', $longitude)
            ->where('latitude', '=', $latitude)
            ->count();
        // if location exists
        if ($sql == 1) {
            // return ID
            $locationId = DB::table('Location')
                ->where('longitude', '=', $longitude)
                ->where('latitude', '=', $latitude)
                ->first();
            $locationId = $locationId->id;
        } // add new location entry
        else {
            $locationId = $this->addNewLocation($longitude, $latitude);
        }
        return $locationId;
    }


    public function addNewLocation($longitude, $latitude) {
        $locationId = DB::table('Location')->insertGetId([
            'longitude' => $longitude,
            'latitude' => $latitude
        ]);

        return $locationId;
    }

}