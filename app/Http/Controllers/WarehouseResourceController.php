<?php
/**
 * Created by PhpStorm.
 * User: night5talker
 * Date: 17.01.17.
 * Time: 19:12
 */

namespace App\Http\Controllers;


use App\WarehouseResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WarehouseResourceController extends Controller {

    protected $model;

    public function __construct() {
        $this->model = new WarehouseResource();
    }


    // read all entries
    public function index() {

        $data = $this->model->indexWarehouse();

        return response()->json($data);
    }


    // create new entry and return json response
    public function create(Request $request) {
        // must be key => value to compare keys later
        $mandatory = array(
            'name' => '',
            'managerId' => '',
            'latitude' => '',
            'longitude' => ''
        );
        // check all params in request
        if (array_diff_key($mandatory, $request->all())) {
            $data['data'] = 'Missing parameter in request';
            $data['response_code'] = 400;
            Log::error('Bad request incoming '.$request);
        } else {
            $data = $this->model->createWarehouse($request->all());
        }

        return response()->json($data);
        return response()->json(['data' => $data['data'], 'Response Code' => $data['response_code']]);
    }


    // read single entry
    public function show($id) {

        $data = $this->model->showWarehouse($id);

        // return json data
        return response()->json(['data' => $data['data'], 'Response Code' => $data['response_code']]);
    }


    // update existing entry - find by ID
    public function store(Request $request, $id) {
    // must be key => value to compare keys later
        $mandatory = array(
            'name' => '',
            'managerId' => '',
            'latitude' => '',
            'longitude' => ''
        );


        // check all params in request
        if (array_diff_key($mandatory, $request->all())) {
            $data['data'] = 'Missing parameter in request';
            $data['response_code'] = 400;
            Log::error('Bad request incoming '.$request);
        } else {

            $data = $this->model->updateWarehouse($request->all(), $id);
        }
        return response()->json(['data' => $data['data'], 'Response Code' => $data['response_code']]);
    }


    // delete single entry - find by ID
    public function delete($id) {
        $data = $this->model->deleteWarehouse($id);

        return response()->json(['data' => $data['data'], 'Response Code' => $data['response_code']]);
    }
}