<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

/*
 * API EXAMPLE BODY
 *
 * {"name":"test","managerId":1,"latitude":45.5785779,"longitude":18.6955140}
 *
 * don't forget use prefix in URI
 */
$app->group(['prefix' => 'api'], function ($app) {

    // create
    $app->post('WarehouseResource', 'WarehouseResourceController@create');

    // update
    $app->put('WarehouseResource/{id}', 'WarehouseResourceController@store');
    // delete
    $app->delete('WarehouseResource/{id}', 'WarehouseResourceController@delete');

    // read single entrie
    $app->get('WarehouseResource/{id}', 'WarehouseResourceController@show');
    $app->get('WarehouseResource', 'WarehouseResourceController@index');

});